const customBar = require("custom-electron-titlebar");

let Titlebar = new customBar.Titlebar({
    backgroundColor: customBar.Color.fromHex('#404040'),
    shadow: false,
    icon: '../icon/favicon.ico'
});
Titlebar.updateTitle('Borcode');
const electron = require("electron");
const {app, BrowserWindow, Menu, ipcMain, dialog} = electron;

function createWindow() {
    let window = new BrowserWindow({
        width: 1280,
        height: 720,
        icon: './src/icon/favicon.ico',
        frame: false,
        webPreferences: {
            nodeIntegration: true
        }
    })
    window.loadFile('./src/welcome/welcome.html');
}




app.on('ready', () => {
    createWindow();
    console.log("Welcome!");

    // !! Menu !! //
    const template = [
        {
            label: "File",
            submenu: [
                {label: "New File", accelerator: "CmdOrCtrl+N"}, 
                {label: "New Window"},
                {label: "New Folder"},
                {type: "separator"},
                {label: "Open File", accelerator: "CmdOrCtrl+O"},
                {label: "Open Folder"},
                {type: "separator"},
                {label: "Save File" , accelerator: "CmdOrCtrl+S"},
                {label: "Save As" , accelerator: "CmdOrCtrl+Shift+Z"},
                {label: "Save All"},
                {type: "separator"},
                {label: "Exit", role: "quit", accelerator: "CmdOrCtrl+Q"}
            ]
        },
        {
            label: "Edit",
            submenu: [
                {label: "Undo", accelerator: "CmdOrCtrl+Z", role: "undo"},
                {label: "Redo", accelerator: "CmdOrCtrl+Y", role: "redo"},
                {type: "separator"},
                {label: "Copy" , accelerator: "CmdOrCtrl+C", role: "copy"},
                {label: "Cut", accelerator: "CmdOrCtrl+X", role: "cut"},
                {label: "Paste" , accelerator: "CmdOrCtrl+V", role: "paste"}
            ]
        },
        {
            label: "View",
            submenu: [
                {label: "Zoom In", role: "zoomIn"},
                {label: "Zoom Out", role: "zoomOut"},
                {type: "separator"},
                {label: "Toggle Full Screen", accelerator: "F11", role: "togglefullscreen"}
            ]
        },
        {
            label: "Project",
            submenu: [
                {label: "Open Project"},
                {label: "Switch Project"},
            ]
        }

    ]
    const menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(menu);    
})